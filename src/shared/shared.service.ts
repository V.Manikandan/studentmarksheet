import { Injectable } from '@nestjs/common';
import { readFileSync, writeFileSync } from 'fs';
import * as path from 'path';
import { CreateStudentDto } from 'src/student/dto/create-student.dto';
@Injectable()
export class SharedService {
  constructor() {}

  readAndParseDBFile = () => {
    return JSON.parse(
      readFileSync(path.resolve(__dirname, process.env.FILE_PATH), {
        encoding: 'utf8',
        flag: 'r',
      }),
    );
  };
  writeDBFile = (dbData: CreateStudentDto[]) => {
    try {
      writeFileSync(
        path.resolve(__dirname, process.env.FILE_PATH),
        JSON.stringify(dbData),
        {
          encoding: 'utf8',
        },
      );
      return dbData;
    } catch (error) {
      return error;
    }
  };
}
