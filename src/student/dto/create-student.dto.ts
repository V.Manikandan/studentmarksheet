export class CreateStudentDto {
  student_name: string;
  student_id?: string;
  subject_1: number;
  subject_2: number;
  subject_3: number;
  subject_4: number;
  subject_5: number;
}
