import { PartialType } from '@nestjs/mapped-types';
import { CreateStudentDto } from './create-student.dto';

export class UpdateStudentDto extends PartialType(CreateStudentDto) {
  student_id: string;
  subject_1?: number;
  subject_2?: number;
  subject_3?: number;
  subject_4?: number;
  subject_5?: number;
}
