import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { SharedService } from 'src/shared/shared.service';
import { DeleteStudentDTO } from './dto/delete-student.dto';

@Injectable()
export class StudentService {
  constructor(private sharedService: SharedService) {}
  create(createStudentDto: CreateStudentDto) {
    const dbData: CreateStudentDto[] = this.sharedService.readAndParseDBFile();
    createStudentDto['student_id'] = Date.now().toString();
    dbData.push(createStudentDto);
    return this.sharedService.writeDBFile(dbData);
  }

  findAll() {
    const dbData: CreateStudentDto[] = this.sharedService.readAndParseDBFile();
    if (!dbData.length)
      throw new HttpException('No Records Found', HttpStatus.NOT_FOUND);
    return dbData;
  }

  findOne(id: number) {
    return `This action returns a #${id} student`;
  }

  update(updateStudentDto: UpdateStudentDto) {
    const dbData: CreateStudentDto[] = this.sharedService.readAndParseDBFile();
    const to_be_updated_student_data = dbData.filter(
      each_student => each_student.student_id === updateStudentDto.student_id,
    );
    const removed_data = dbData.filter(
      each_student => each_student.student_id != updateStudentDto.student_id,
    );
    if (!to_be_updated_student_data)
      throw new HttpException('Student Id not found', HttpStatus.NOT_FOUND);

    for (const subject in to_be_updated_student_data[0]) {
      if (subject in updateStudentDto)
        to_be_updated_student_data[0][subject] = updateStudentDto[subject];
    }
    removed_data.push(to_be_updated_student_data[0]);
    return this.sharedService.writeDBFile(removed_data);
  }

  remove(deleteStudentDto: DeleteStudentDTO) {
    const dbData: CreateStudentDto[] = this.sharedService.readAndParseDBFile();
    const to_be_deleted_student_data = dbData.filter(
      each_student => each_student.student_id === deleteStudentDto.student_id,
    );
    if (!to_be_deleted_student_data.length)
      throw new HttpException('No Record found', HttpStatus.NOT_FOUND);
    const removed_data = dbData.filter(
      each_student => each_student.student_id != deleteStudentDto.student_id,
    );
    return this.sharedService.writeDBFile(removed_data);
  }
}
