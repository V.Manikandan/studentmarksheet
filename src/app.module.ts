import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StudentModule } from './student/student.module';
import { SharedModule } from './shared/shared.module';

@Module({
  imports: [StudentModule, SharedModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
